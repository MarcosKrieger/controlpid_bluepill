# ControlPID_BluePill

Como proyecto final de las materias Técnicas Digitales 3 y Sitemas de control de la UTN-FRC, se implemtó un control proporcional, integral y derivativo (PID) para controlar la temperatura de una planta en tiempo real utilizando un microcontrolador STM32f103c8t6 (Blue Pill).

Además se agregó un UI para observar y controlar diferentes parámetros de la planta.
